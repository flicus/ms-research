package com.jnetx.config;

public enum ConfigChangeType {
    ADDED,
    REMOVED,
    MODIFIED
}
