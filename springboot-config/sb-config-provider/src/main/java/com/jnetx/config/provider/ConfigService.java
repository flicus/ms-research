package com.jnetx.config.provider;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.jnetx.config", "com.jnetx.config.example"})
public class ConfigService {
}