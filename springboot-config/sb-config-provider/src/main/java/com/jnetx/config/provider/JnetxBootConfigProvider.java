package com.jnetx.config.provider;

import com.jnetx.container.AbstractProvider;
import com.jnetx.container.ProviderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.AssignableTypeFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class JnetxBootConfigProvider extends AbstractProvider {

    private static final Logger log = LoggerFactory.getLogger("config-provider");

    private ConfigurableApplicationContext ctx;

    private List<Class<?>> applications;

    public JnetxBootConfigProvider() {
    }


    @Override
    protected void doInit() throws ProviderException {
        super.doInit();
        applications = new ArrayList<>();
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(JnetxApp.class));
        for (BeanDefinition bd : scanner.findCandidateComponents("com.jnetx.config.provider")) {
            try {
                applications.add(Class.forName(bd.getBeanClassName()));
            } catch (ClassNotFoundException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    protected void doStart() throws ProviderException {
        super.doStart();

        log.debug("onStart: ", applications.toArray());

        SpringApplicationBuilder builder = new SpringApplicationBuilder(ConfigService.class);
        builder
                .parent(ConfigService.class)
                .web(WebApplicationType.NONE);

        for (Class cls : applications) {
            JnetxApp jnetxApp = (JnetxApp) cls.getAnnotation(JnetxApp.class);
            Properties p = new Properties();
            p.put("spring.application.name", jnetxApp.appName());
            builder
                    .child(cls)
                    .web(WebApplicationType.REACTIVE)
                    .application()
                    .setDefaultProperties(p);
        }
        ctx = builder.run(new String[]{});
    }

    @Override
    protected void doStop() throws ProviderException {
        super.doStop();
        ctx.close();
    }
}
