package com.jnetx.config.provider.clients;

import com.jnetx.config.provider.JnetxApp;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = {"com.jnetx.config", "com.jnetx.config.example"})
@EnableScheduling
@JnetxApp(appName = "example")
public class ExampleConfigService {
}
