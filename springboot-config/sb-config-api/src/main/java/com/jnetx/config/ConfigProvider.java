package com.jnetx.config;

public interface ConfigProvider {

    public Object getConfiguration();
}
