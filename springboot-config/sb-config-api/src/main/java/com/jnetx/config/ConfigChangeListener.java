package com.jnetx.config;

public interface ConfigChangeListener {

    public void configChanged(ConfigChange[] changes);

}


