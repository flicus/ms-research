package com.jnetx.config.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = {"com.jnetx.config", "com.jnetx.config.example"})
@EnableScheduling
public class ExampleConfigService extends SpringBootServletInitializer {

    private static Class<ExampleConfigService> applicationClass = ExampleConfigService.class;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(applicationClass);
    }

    public static void main(String[] args) {
        SpringApplication.run(ExampleConfigService.class, args);
    }
}