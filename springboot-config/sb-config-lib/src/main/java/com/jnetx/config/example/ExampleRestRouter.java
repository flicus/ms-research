package com.jnetx.config.example;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

@Configuration
@RefreshScope
public class ExampleRestRouter {

    @Value("${example.key:DefaultKey}")
    private String value;

    @Autowired
    private ExampleConfig config;

    @Bean
    public RouterFunction<ServerResponse> getExampleConfig() {
        return route(GET("/config"),
                serverRequest -> ok().body(Mono.just(config), ExampleConfig.class));
    }

    @Bean
    public RouterFunction<ServerResponse> getConfig() {
        return route(GET("/value"),
                serverRequest -> ok().body(Mono.just(value), String.class));
    }

}
