package com.jnetx.config.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.jnetx.config.ConfigChange;
import com.jnetx.config.ConfigChangeListener;
import com.jnetx.config.ConfigChangeType;
import com.jnetx.config.ConfigProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Component
public class ConfigRefresher {

    private static final Logger log = LoggerFactory.getLogger("config-refresher");

    private ObjectMapper mapper = new ObjectMapper();
    private DocumentContext currentConfigContext;

    @Autowired
    private Environment environment;

    @Value("${spring.application.server.port}")
    private String localServerPort;

    @Value("${spring.application.name}")
    private String appName;

    @Autowired
    private ConfigProvider configProvider;

    @Autowired
    private List<ConfigChangeListener> listeners;

    public ConfigRefresher(ConfigProvider configProvider) {
        this.configProvider = configProvider;
        try {
            currentConfigContext = JsonPath.parse(mapper.writeValueAsString(configProvider.getConfiguration()));
        } catch (JsonProcessingException e) {
            log.error("Cannot initialize config context", e);
        }
    }

    private void notifyListeners(ConfigChange[] changes) {
        if (listeners != null) {
            listeners.forEach(l -> l.configChanged(changes));
        }
    }

    @Scheduled(fixedRate = 30000)
    public void refresh() {
        String[] changedPaths = invokeActuatorRefresh();
        boolean isChanged = changedPaths != null && changedPaths.length > 0;

        if (!isChanged) {
            return;
        }

        DocumentContext newConfigContext = null;
        try {
            newConfigContext = JsonPath.parse(mapper.writeValueAsString(configProvider.getConfiguration()));
        } catch (Exception e) {
            log.error("Cannot analyze config", e);
            return;
        }

        log.info("### Changed paths: " + Arrays.asList(changedPaths));
        ConfigChange[] changes = new ConfigChange[changedPaths.length];
        for (int i = 0; i < changedPaths.length; i++) {
            String changedPath = changedPaths[i];
            if (changedPath.startsWith("example-config.")) {
                changedPath = changedPath.substring(changedPath.indexOf('.') + 1);
            }

            Object oldObject = null;
            try {
                oldObject = this.currentConfigContext.read(changedPath);
            } catch (PathNotFoundException e) {
                // ignore
            }

            Object newObject = null;
            try {
                newObject = newConfigContext.read(changedPath);
            } catch (PathNotFoundException e) {
                // ignore
            }

            if (newObject == null) {
                changes[i] = new ConfigChange(ConfigChangeType.REMOVED, changedPath, oldObject, null);
            } else if (oldObject == null) {
                changes[i] = new ConfigChange(ConfigChangeType.ADDED, changedPath, null, newObject);
            } else {
                changes[i] = new ConfigChange(ConfigChangeType.MODIFIED, changedPath, oldObject, newObject);
            }
        }

        // reset current config context
        this.currentConfigContext = newConfigContext;

        // notify listeners
        if (changes.length > 0) {
            notifyListeners(changes);
        }
    }

    private String[] invokeActuatorRefresh() {
        // curl -X POST localhost:8080/actuator/refresh -d {} -H "Content-Type: application/json"
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        HttpEntity<String> request = new HttpEntity<>("{}", headers);
        ResponseEntity<String> response = restTemplate.exchange(String.format("http://%s:%s/%s/actuator/refresh", "localhost", localServerPort, appName),
                HttpMethod.POST,
                request,
                String.class);

        System.out.println("Actuator refresh response: " + response);
        log.debug("Actuator refresh response: " + response);

        if (!response.getStatusCode().is2xxSuccessful()) {
            log.warn("Actuator response not successful: " + response);
        }

        String[] changedPaths = null;
        String responseBody = response.getBody();
        try {
            changedPaths = mapper.readValue(responseBody, String[].class);
        } catch (IOException e) {
            log.error("Cannot deserialize change: " + responseBody, e, e);
        }
        return changedPaths;
    }
}
