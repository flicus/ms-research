package com.jnetx.config.example;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.jnetx.config.ConfigChange;
import com.jnetx.config.ConfigChangeListener;

import static com.jnetx.config.ConfigChangeType.*;

@Component
public class ExampleConfigChangeListener implements ConfigChangeListener {

    private static final Logger log = LoggerFactory.getLogger("example-config-listener");

    @Override
    public void configChanged(ConfigChange[] changes) {
        Arrays.asList(changes).forEach(c -> {
            switch (c.getType()) {
                case ADDED:
                    log.info("### ADDED: " + c.getObjectNew());
                    break;
                case REMOVED:
                    log.info("### REMOVED: " + c.getObjectOld());
                    break;
                case MODIFIED:
                    log.info("### MODIFIED: " + c.getObjectOld() + " -> " + c.getObjectNew());
                    break;
            }
        });
    }
}
