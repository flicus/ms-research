package com.jnetx.config.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jnetx.config.ConfigProvider;

@Component
public class ExampleConfigProvider implements ConfigProvider {

    @Autowired
    private ExampleConfig exampleConfig;

    @Override
    public Object getConfiguration() {
        return exampleConfig;
    }
}
