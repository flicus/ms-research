package com.jnetx.config.example;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class ExampleServerConfig implements Serializable {

    private String host;

    private int port;

    public ExampleServerConfig() {
    }

    @JsonProperty
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @JsonProperty
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "ExampleServerConfig {" +
                "host='" + host + '\'' +
                ", port=" + port +
                '}';
    }
}
