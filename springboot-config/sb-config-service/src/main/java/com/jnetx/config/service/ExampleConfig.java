package com.jnetx.config.service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties(prefix = "example-config")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class ExampleConfig implements Serializable {

    private int timeout;

    private List<ExampleServerConfig> servers = new ArrayList<>();

    public ExampleConfig() {
    }

    @JsonProperty
    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    @JsonProperty
    public List<ExampleServerConfig> getServers() {
        return servers;
    }

    public void setServers(List<ExampleServerConfig> servers) {
        this.servers = servers;
    }

    @Override
    public String toString() {
        return "ExampleConfig {" +
                "timeout=" + timeout +
                ", servers=" + servers +
                '}';
    }
}
