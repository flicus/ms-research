package com.jnetx.config.client;

public class ConfigChange {

    private ConfigChangeType type;
    private String path;
    private Object objectOld;
    private Object objectNew;

    public ConfigChange(ConfigChangeType type, String path, Object objectOld, Object objectNew) {
        this.type = type;
        this.path = path;
        this.objectOld = objectOld;
        this.objectNew = objectNew;
    }

    public ConfigChangeType getType() {
        return type;
    }

    public String getPath() {
        return path;
    }

    public Object getObjectOld() {
        return objectOld;
    }

    public Object getObjectNew() {
        return objectNew;
    }

    @Override
    public String toString() {
        return "ConfigChange {" +
                "type=" + type +
                ", path='" + path + '\'' +
                ", objectOld=" + objectOld +
                ", objectNew=" + objectNew +
                '}';
    }
}
