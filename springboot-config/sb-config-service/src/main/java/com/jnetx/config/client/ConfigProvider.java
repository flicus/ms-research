package com.jnetx.config.client;

public interface ConfigProvider {

    public Object getConfiguration();
}
