package com.jnetx.config.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class ExampleRestController {

    @Value("${example.key:DefaultKey}")
    private String value;

    @Autowired
    private ExampleConfig config;

    @RequestMapping(path = "/rest/example")
    public String printValue() {
        return value;
    }

    @RequestMapping(path = "/rest/example/config", produces = MediaType.APPLICATION_JSON_VALUE)
    public ExampleConfig printConfig() {
        return config;
    }

}
