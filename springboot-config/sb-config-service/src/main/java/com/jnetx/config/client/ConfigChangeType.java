package com.jnetx.config.client;

public enum ConfigChangeType {
    ADDED,
    REMOVED,
    MODIFIED
}
