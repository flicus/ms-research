package com.jnetx.config.client;

public interface ConfigChangeListener {

    public void configChanged(ConfigChange[] changes);

}


