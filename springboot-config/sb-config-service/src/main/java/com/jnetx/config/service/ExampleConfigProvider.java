package com.jnetx.config.service;

import com.jnetx.config.client.ConfigProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExampleConfigProvider implements ConfigProvider {

    @Autowired
    private ExampleConfig exampleConfig;

    @Override
    public Object getConfiguration() {
        return exampleConfig;
    }
}
